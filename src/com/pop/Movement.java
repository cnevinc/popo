package com.pop;

import java.util.ArrayList;
import java.util.HashMap;

import com.pop.core.Core;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class Movement extends Activity {

	private static final String TAG = "POPO_DEBUG";
	ListView mDisplayedLogList;
	ArrayList<HashMap<String, String>> mSavedLogs = new ArrayList<HashMap<String, String>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movement);

		// Initial the APP with .load() function
		Core.setContext(Movement.this);
		Core.load();
		Core system = Core.getCoreInstance();
		
		// Retain the log list saved and put it in ArrayList, and then
		// initialize the list view element
		this.mSavedLogs = system.getLogList();
		if (mSavedLogs == null) {
			mSavedLogs = new ArrayList<HashMap<String, String>>();
		}
		mDisplayedLogList = (ListView) findViewById(R.id.listView1);
		Log.d(TAG, "mLogList [" + mSavedLogs.size() + "] mListView[" + mDisplayedLogList
				+ "]");

		// Define action after "Remove" is clicked
		mDisplayedLogList.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				AlertDialog.Builder adb = new AlertDialog.Builder(Movement.this);
				// TODO: i18n stuff and externalize string
				adb.setTitle("Delete?");
				adb.setMessage("�T�w�R�� ?");
				final int positionToRemove = position;
				adb.setNegativeButton("Cancel", null);
				adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// remove the item been clicked and refresh the view
						mSavedLogs.remove(positionToRemove);
						mDisplayedLogList.invalidateViews();

					}
				});
				adb.show();
				return false;
			}
		});
		// Bind data and view element
		mDisplayedLogList.setAdapter((ListAdapter) new SimpleAdapter(this, mSavedLogs,
				android.R.layout.simple_list_item_1, new String[] { "date" },
				new int[] { android.R.id.text1 }));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; This adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_movement, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.menu_movement:
			java.util.Date now = new java.util.Date();
			HashMap<String, String> newItem = new HashMap<String, String>();
			newItem.put("date", now.toLocaleString());
			mSavedLogs.add(newItem);
			mDisplayedLogList.invalidateViews();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		Core.getCoreInstance().setLogList(mSavedLogs);
		Core.getCoreInstance().save();
	}

}
