package com.pop.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;

public class Core implements Serializable {

	public static final String TAG = "POPO_DEBUG";
	private static final long serialVersionUID = 1L;

	private static Core mCore;
	private static String fileName = "core.ser";

	transient private static Context mContext;

	private ArrayList<HashMap<String, String>> mLogList;

	// Private constructor prevent user to ensure singleton
	private Core() {

	}

	public static synchronized Core getCoreInstance() {
		if (mCore == null) {
			mCore = new Core();
		}
		return mCore;
	}

	public static void setContext(Context context) {
		mContext = context;
	}

	// Save the configuration to SD card
	public static void save() {
		FileOutputStream fos = null;
		ObjectOutputStream os = null;
		try {
			fos = mContext.openFileOutput(fileName, Context.MODE_PRIVATE);

			if (fos == null) {
				// TODO:i18n stuff and externalize string
				Log.e(TAG, " Core save but null file");
				return;
			}
			os = new ObjectOutputStream(fos);
			os.writeObject(mCore);
			// TODO:i18n stuff and externalize string
			Log.d(TAG, " Core saved: " + mCore.toString());
		} catch (FileNotFoundException e) {
			Log.e(TAG, " ClassNotFoundException when saving configuration : "
					+ e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG,
					"IOException when saving configuration : " + e.toString());
			e.printStackTrace();
		} finally {
			try {
				if (fos != null)
					fos.close();
				if (os != null)
					os.close();
			} catch (IOException e) {
				Log.e(TAG, "IOException closing os " + e);
			}

		}

	}

	// Load the configuration from SD card
	public static void load() {
		ObjectInputStream is = null;
		try {
			FileInputStream fis = mContext.openFileInput(fileName);
			is = new ObjectInputStream(fis);
			mCore = (Core) is.readObject();
			// TODO:i18n stuff and externalize string
			Log.d(TAG, " Core loaded: " + mCore.toString());
		} catch (FileNotFoundException e) {
			Log.e(TAG, " File Not Found ExceptionS:" + e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			Log.e(TAG, " IOExceptionS :" + e.toString());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			Log.e(TAG, " ClassNotFoundExceptionS:" + e.toString());
			e.printStackTrace();

		} finally {
			try {
				if (is != null)
					is.close();
			} catch (IOException e) {
				Log.e(TAG,
						" IOException when loading the config " + e.toString());
			}
		}

	}

	public ArrayList<HashMap<String, String>> getLogList() {
		return mLogList;
	}

	public void setLogList(ArrayList<HashMap<String, String>> mLogList) {
		this.mLogList = mLogList;
	}

}

