package com.pop.core;

import java.io.Serializable;
import java.util.Properties;

public class Profile implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mLogModfiedDate;

	public String getDate() {
		return mLogModfiedDate;
	}

	public void setDate(String date) {
		this.mLogModfiedDate = date;
	}

}
